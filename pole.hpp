#ifndef POLE_H
#define POLE_H

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <Windows.h>
#include <conio.h>
using namespace std;

enum status {cell,empty};                 //cell - есть. empty - нет
const int COLUMNS=77;  //77               //число столбцов
const int ROWS=20;     //20               //число строк

struct Coordinates{
    int x;
    int y;
    Coordinates(int x, int y) {
        this->x=x;
        this->y=y;
    }
};

class Pole{
   char kletka;
   status data[ROWS][COLUMNS];
public:
   status* Get(int x, int y);             //получить ячейку
public:
   Pole(char kletka);
   void Show();
   void Change(status add, int x, int y); //изменить значение
   int  CountNeighbors(int x, int y);     //число соседей ячейки
   void CheckCell();                      //проверка и замена ячеек
   void RandomFill(double p);             //случайное заполнение
   void FileFill();                       //заполнение из файла
};


#endif // POLE_H
