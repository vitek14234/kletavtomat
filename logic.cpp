#include "pole.hpp"
#include <vector>

void Pole::CheckCell(){
    vector<Coordinates> DeadList; //список умирающих клеток
    vector<Coordinates> LifeList; //списое новых клеток
    Coordinates *temp;
    for(int x=0;x<COLUMNS;x++)
        for(int y=0;y<ROWS;y++){
            if (CountNeighbors(x,y)==3) {
                temp=new Coordinates(x,y);
                LifeList.push_back(*temp);
                delete temp;
            }
            else if(((CountNeighbors(x,y)<2)||(CountNeighbors(x,y)>3))&&(*Get(x,y))!=empty)
            {
                temp=new Coordinates(x,y);
                DeadList.push_back(*temp);
                delete temp;
            }

        }
    for(int i=0;i<DeadList.size();i++){
        Change(empty, DeadList[i].x, DeadList[i].y); //убить клетки
    }
    for(int i=0;i<LifeList.size();i++)
        Change(cell, LifeList[i].x, LifeList[i].y);  //родить клетки

}
