#include "pole.hpp"
#include <cstdlib>
Pole::Pole(char kletka){
    this->kletka=kletka;
    for(int i=0;i<ROWS;++i)
       for(int j=0;j<COLUMNS;++j)
          data[i][j]=empty;
}
status* Pole::Get(int x, int y){
   if (x<0) x=COLUMNS+x;
   if (x>=COLUMNS) x=x-COLUMNS;
   if (y<0) y=ROWS+y;
   if (y>=ROWS) y=y-ROWS;
   status *temp=&data[y][x];
   return temp;
}

void Pole::Change(status add, int x, int y){
    *Get(x,y)=add;
}

int Pole::CountNeighbors(int x, int y){
    int count=0;
    for(int i=x-1;i<=x+1;i++)
        for(int j=y-1;j<=y+1;j++)
            if (*Get(i,j)==cell) count++;
    if(*Get(x,y)==cell) count--;
    return count;
}

void Pole::Show(){
    cout<<(char)218;
    for(int j=0;j<COLUMNS;j++){
        cout<<(char)196;
    }
    cout<<(char)191<<endl;
    for(int i=0;i<ROWS;i++){
        cout<<(char)179;
        for(int j=0;j<COLUMNS;j++){
            if (data[i][j]==empty)
                cout<<" ";
            else cout<<kletka;
        }
        cout<<(char)179<<endl;
    }
    cout<<(char)192;
    for(int j=0;j<COLUMNS;j++){
        cout<<(char)196;
    }
    cout<<(char)217<<endl;
}
void Pole::RandomFill(double p){
    for(int i=0;i<ROWS;++i)
       for(int j=0;j<COLUMNS;++j)
       {
          if((double)rand()/RAND_MAX<p) data[i][j]=cell;
       }
}
void Pole::FileFill(){
    ifstream in("input.txt");
    for(int i=0;i<ROWS;++i){
        string str;
        in>>str;
       for(int j=0;j<COLUMNS;++j)
       {
           if (str[j]=='1') data[i][j]=cell;
           else data[i][j]=empty;
       }
    }
}
