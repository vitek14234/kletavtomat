#include "pole.hpp"
#include <unistd.h>
int main()
{
    cout<<"Select a symbol for cell"<<endl;
    cout<<"1-"<<'o'<<endl;
    cout<<"2-"<<'*'<<endl;
    cout<<"3-"<<'#'<<endl;
    cout<<"4-"<<(char)219<<endl;
    int number=-1;
    int fill=-1;
    Pole *a;
    do{
        cin>>number;
        switch (number){
        case 1:
          a=new Pole('o');
          break;
        case 2:
          a=new Pole('*');
            break;
        case 3:
          a=new Pole('#');
            break;
        case 4:
          a=new Pole((char)219);
            break;
     }
    }
    while((number<1)||(number>4));


    cout<<"Select the method of filling"<<endl;
    cout<<"1-"<<"Random"<<endl;
    cout<<"2-"<<"From file"<<endl;

    do{
        cin>>fill;
        switch (fill){
        case 1:{
          double p=0;
          cout<<"Enter the probability of occurrence"<<endl;
          cin>>p;
          if((p>1)||(p<0)) p=0.5;
          a->RandomFill(p);
          break;
        }
        case 2:{
          a->FileFill();
          break;
        }
     }
    }
    while((fill<1)||(fill>2));

    for(int i=0;i<10000;i++){
    system("cls");
    a->Show();
    a->CheckCell();
    cout<<i;
    Sleep (512);  //скорость
    }
    return 0;

}

