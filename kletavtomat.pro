TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    pole.cpp \
    logic.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    pole.hpp

